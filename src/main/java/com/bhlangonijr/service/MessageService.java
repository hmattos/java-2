package com.bhlangonijr.service;

import com.bhlangonijr.domain.Message;
import com.bhlangonijr.domain.Response;
import com.bhlangonijr.repository.MessageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MessageService {
    private static final Logger log = LoggerFactory.getLogger(MessageService.class);
    private static final String MESSAGE_SUCCESS = "hello there - has been processed";
    private static final String MESSAGE_ID_SUCCESS = "1";
    private static final String MESSAGE_ERROR_MARS = "Message cannot be sourced by martians!";
    private static final String MESSAGE_ID_ERROR_MARS = "2";

    private MessageRepository messageRepository;

    public MessageService() {
        messageRepository = new MessageRepository();
    }

    /**
     * Validate and store this message into the database
     *
     * @param message
     * @return
     * @throws Exception
     */
    public Response send(Message message) throws Exception {
        log.info(">> send : " + message);

        Response response = new Response();

        try {
            if (!message.getFrom().contains("@mars")) {
                response = messageRepository.save(message);
                response.setText(MESSAGE_SUCCESS);
                response.setMessageId(MESSAGE_ID_SUCCESS);
                log.info("<< send: Success");
                return response;
            } else {
                response.setText(MESSAGE_ERROR_MARS);
                response.setMessageId(MESSAGE_ID_ERROR_MARS);
                response.setSuccess(Boolean.FALSE);
                log.info("<< send: Success");
                return response;
            }
        }catch (Exception e){
            throw new Exception(e.getMessage());
        }

    }

    public List<Message> getAllMessages() {
        return messageRepository.getAll();
    }

    public Message getById(String id) {
        return messageRepository.getById(id);
    }


}
