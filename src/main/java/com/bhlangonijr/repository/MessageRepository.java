package com.bhlangonijr.repository;

import com.bhlangonijr.domain.Message;
import com.bhlangonijr.domain.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.util.ArrayList;
import java.util.List;

/**
 * Classe Criada para ser a fonte de dados da aplicacao.
 */
public class MessageRepository {
    private static final Logger log = LoggerFactory.getLogger(MessageRepository.class);

    private List<Message> messages;

    public MessageRepository(){
        this.messages = new ArrayList<Message>();
    }

    public Response save(Message message){
        log.info(">> save : " + message);
        this.messages.add(message);
        Response response = new Response();
        response.setSuccess(Boolean.TRUE);
        log.info("<< save : Success");
        return response;
    }

    public List<Message> getAll(){
        log.info(">> getAll : ");
        return this.messages;
    }

    public Message getById(String id){
        log.info(">> getById : " + id);
        for (Message message : this.messages ) {
            if (message.getId().equals(id)){
                log.info("<< getById : " + message);
                return message;
            }
        }
        log.info("<< getById : null");
        return null;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }
}
